<?php namespace App\myFrameWorkPHP
{
    //Clase abtracta, padre de la Clase SqlMySql
    abstract class AbstractConnection
    {
        protected $connections;

        abstract public function connection();
        abstract public function disconnect();
        abstract public function executeQuery($query);
        //abstract public function nextResultRow($result);
    }
}




